# -*- coding: utf-8 -*-

from odoo import models, fields, api

class res_partner_property(models.Model):
   
    _inherit = 'res.partner'

    ruc = fields.Integer(string='DNI o RUC')
    properting  = fields.Boolean('propietario')
    leasing     = fields.Boolean('Arrendatario')
    contract_id = fields.One2many('property.contract','lessee','Contractos')

      
      