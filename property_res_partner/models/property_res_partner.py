# -*- coding: utf-8 -*-

from odoo import models, fields, api

class property_property(models.Model):
    _name = 'property.res.partner'

    name = fields.Many2one('res.partner', 'Propietario')
    participation = fields.Float('Participacion')
    status= fields.Boolean('Estatus')
    property_id = fields.Many2one('property.property')