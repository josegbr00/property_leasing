# -*- coding: utf-8 -*-
{
    'name': "Arrendamiento ",

    'summary': """
       Modulo para gestion de condominio o arrendamiento""",

    'description': """
         Modulo para gestion de condominio o arrendamiento
    """,

    'author': "ING. Jose Blanco",
    'website': "http://www.joseblanco.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'administration',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','product'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/property.xml',
        'views/property_leasing.xml',
        'views/property_contract.xml',
        'views/property_payment.xml',
        'views/property_type.xml',
        'views/property_payment_out.xml',
        'views/property_type_service.xml',
        'views/property_date.xml',
        'views/property_charge.xml',
        'report/report_payment.xml',
        #'report/report_charge.xml',
         'data/property.property.csv',
         'data/property_type.property.csv',
         'data/property.leasing.csv',
         'data/property_type_services.property.csv',
         'data/property_date.property.csv',
        # 'data/res.partner.csv'
        
    ],
    'sequence': 1,
    'installable': True,
    'application': True,
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}