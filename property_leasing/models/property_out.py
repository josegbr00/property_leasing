# -*- coding: utf-8 -*-

from odoo import models, fields, api,time

class property_out(models.Model):
    _name = 'payment_out.property'

    name            =  fields.Char( string="N° Pago")
    currency_id     =  fields.Many2one('res.currency', string='Tipo de Moneda')
    amount          =  fields.Monetary(string="Monto a Pagar", currency_field='currency_id')
    type_services   =  fields.Many2one('property_type_services.property', string='Tipo de Servicio') 
    payment_month   =  fields.Selection(
        string='Mes',
        selection=[ ('Enero', 'Enero'), 
                    ('Febrero', 'Febrero'),
                    ('Marzo', 'Marzo'),
                    ('Abril', 'Abril'),
                    ('Mayo', 'Mayo'),
                    ('Junio', 'Junio'),
                    ('Julio', 'Julio'),
                    ('Agosto', 'Agosto'),
                    ('Septiembre', 'Septiembre'),
                    ('Octubre', 'Octubre'),
                    ('Noviembre', 'Noviembre'),
                    ('Diciembre', 'Diciembre'),
                    ])
    description     =  fields.Text("Descripcion")
    
    @api.onchange('payment_month')
    def _set_name_contract(self):
        
        for record in self: 
            if record.payment_month != False and record.type_services.name != False:
                record.name = "PG/%s/%s/%s" %( record.payment_month ,time.strftime("%Y") ,record.type_services.name)
 
