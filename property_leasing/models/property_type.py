# -*- coding: utf-8 -*-

from odoo import models, fields, api

class property_type(models.Model):
    _name = 'property_type.property'

    name = fields.Char( string="Tipo de Inmuebles")
    description = fields.Text( string="Descripcion")
    state_property = fields.Boolean("Activo") 

class property_type_services(models.Model):
    _name = 'property_type_services.property'

    name = fields.Char( string="Tipo de Servico")
    description = fields.Text( string="Descripcion")
    state_property = fields.Boolean("Activo") 

class property_date(models.Model):
    _name = 'property_date.property'

    name =  fields.Selection(
        string='Mes',
        selection=[ ('Enero', 'Enero'), 
                    ('Febrero', 'Febrero'),
                    ('Marzo', 'Marzo'),
                    ('Abril', 'Abril'),
                    ('Mayo', 'Mayo'),
                    ('Junio', 'Junio'),
                    ('Julio', 'Julio'),
                    ('Agosto', 'Agosto'),
                    ('Septiembre', 'Septiembre'),
                    ('Octubre', 'Octubre'),
                    ('Noviembre', 'Noviembre'),
                    ('Diciembre', 'Diciembre'),
                    ])
    date_year = fields.Selection(
        string='Año',
        selection=[('2019', '2019'), ('2020', '2020')]
    )
    status_value = fields.Boolean(
        string='estatus',
    )
    
    