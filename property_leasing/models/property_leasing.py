# -*- coding: utf-8 -*-

from odoo import models, fields, api

class property_leasing(models.Model):
    _name = 'property.leasing'

    name = fields.Char( string="N° Inmueble")
    length = fields.Float( string="Metros Cuadrado")
    property_type = fields.Many2one('property_type.property', 'Tipo Inmueble')
    property_number = fields.Integer(string="Partida Registral")
    contrasts_property  = fields.One2many('property.contract','contrasts_property',
    string='Contrato')
    state_property = fields.Boolean("Estatus")
    property_ids = fields.Many2one('property.property',string='Propiedades')
