# -*- coding: utf-8 -*-

from odoo import models, fields, api,time

class property_charge(models.Model):
    _name = 'property.charge'
    #  property.charge
    name        = fields.Selection(
        string='Mes',
        selection=[ ('Enero', 'Enero'), 
                    ('Febrero', 'Febrero'),
                    ('Marzo', 'Marzo'),
                    ('Abril', 'Abril'),
                    ('Mayo', 'Mayo'),
                    ('Junio', 'Junio'),
                    ('Julio', 'Julio'),
                    ('Agosto', 'Agosto'),
                    ('Septiembre', 'Septiembre'),
                    ('Octubre', 'Octubre'),
                    ('Noviembre', 'Noviembre'),
                    ('Diciembre', 'Diciembre'),
                    ])
    
    year = fields.Integer(
        string=u'Año', 
        default= time.strftime("%Y"),
        
    )
    
    contract_id = fields.Many2one( 'property.contract',string="Contrato")  
    type_charge = fields.Selection(
        string='Tipo de Cobro',
        selection=[('Mantenimiento', 'Mantenimiento'), ('Mensualidad', 'Mensualidad')]
    )
    currency_id = fields.Many2one('res.currency', string='Tipo de Moneda')
    amount      = fields.Monetary(string="Monto",currency_field='currency_id',)
    payment_id  = fields.One2many('payment.property','charge_id',string='Pagos',)

    amount_paid   = fields.Monetary(string="Monto Abonado",
                                    currency_field='currency_id',
                                    compute="_get_total_paid")
    amount_other  = fields.Monetary(string="Monto Pendiente",
                                    currency_field='currency_id',
                                    compute="_get_total_other")

    @api.onchange('type_charge')
    def _get_total_amount(self):
        for record in self:
            if record.contract_id:
                if self.type_charge == 'Mantenimiento':

                    record.currency_id = record.contract_id.currency_id
                    record.amount      = record.contract_id.maintenance 

                if self.type_charge == 'Mensualidad':

                    record.currency_id = record.contract_id.currency_id_two
                    record.amount      = record.contract_id.monthly_payment 
    
    def _get_total_paid(self):
        for record in self:
            if record.payment_id:
                for p in record.payment_id:
                    record.amount_paid +=  p.amaunt_abonar
    
    def _get_total_other(self):
        for record in self:
            if record.payment_id:
                record.amount_other =  record.amount 
                for p in record.payment_id:
                    record.amount_other -=  p.amaunt_abonar
                
