# -*- coding: utf-8 -*-

from odoo import models, fields, api,exceptions,time

class property_payment(models.Model):
    _name = 'payment.property'

    name          = fields.Char( string="N° Pago")
    leasing       = fields.Many2one( 'property.leasing',string="Inmueble")
    contract_id   = fields.Many2one( 'property.contract',string="Contrato") 
    charge_id     = fields.Many2one( 'property.charge',string="Cobro")
    currency_id   = fields.Many2one('res.currency', string='Moneda')
    amount        = fields.Monetary(string="Monto Adeudado",currency_field='currency_id')

# Monto del Cobro 
    currency_amount   = fields.Many2one('res.currency', string='Moneda',)
    charge_amount     = fields.Monetary(string="Monto a Pagar",currency_field='currency_amount')

# Monto a pagar
    amount_total      = fields.Monetary(string="Monto  Restante", currency_field='currency_amount')
    rate              = fields.Monetary(string="tasa", default= 1)
    rate_status       = fields.Boolean(default=False)
    amaunt_abonar     = fields.Monetary(string="Monto a abonar",currency_field='currency_amount')
#Number payment

 
    @api.onchange('charge_id')
    def _set_name_payment(self):
         for record in self: 
            record.name = "RC/%s/%s/%s/%s" %( record.leasing.name  ,record.charge_id.name ,time.strftime("%Y"),len(record.charge_id.payment_id) )
 
    @api.onchange('charge_id')
    def _get_amount(self):
        for record in self:
            if record.charge_id:
                record.currency_amount   =  record.charge_id.currency_id
                record.charge_amount     =  record.charge_id.amount
                if record.charge_id.payment_id :
                     for p in record.charge_id.payment_id :
                         record.charge_amount -= p.amaunt_abonar



    @api.onchange('amount','charge_amount','rate')
    def _get_amount_total(self):
        
        if self.currency_id.id == 163 and self.currency_amount.id == 3:
            self.amaunt_abonar  = self.amount / self.rate 
            self.amount_total =  self.charge_amount - self.amaunt_abonar
        
        if self.currency_id.id == 3 and self.currency_amount.id == 163:
            self.amaunt_abonar  = self.amount * self.rate 
            self.amount_total =  self.charge_amount - self.amaunt_abonar

        if self.currency_id.id ==  self.currency_amount.id:
            self.amaunt_abonar  = self.amount 
            self.amount_total =  self.charge_amount - self.amaunt_abonar

        if  self.charge_amount > 0 and self.amaunt_abonar > 0:
            if self.charge_amount < self.amaunt_abonar:
                raise exceptions.UserError(
                    "El monto abonar es mayo que la deuda"
                )

        
       

    @api.onchange('currency_id','currency_amount')
    def _get_currency(self):
        if self.currency_id != self.currency_amount:
            self.rate_status  = True
        else:
            self.rate_status  = False
        
        
