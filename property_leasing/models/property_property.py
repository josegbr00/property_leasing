# -*- coding: utf-8 -*-

from odoo import models, fields, api

class property_property(models.Model):
    _name = 'property.property'

    name = fields.Char( string="Nombre")
    description = fields.Text( string="Descripción")
    direction = fields.Text(string="Dirección")
    state_property = fields.Boolean("Estatus") 
    property_leasing =  fields.One2many("property.leasing","property_ids",string='Inmuebles')
