# -*- coding: utf-8 -*-

from odoo import models, fields, api,time

class property_contract(models.Model):
    _name = 'property.contract'

    name = fields.Char( string="N° Contrato")
    
    init_date = fields.Date(string="Fecha  Inicio")
    init_end = fields.Date(string="Fecha Término")
    account_contract = fields.Char( string="Pago")

    lessee  = fields.Many2one('res.partner', 'Arrendatario')
    state_contract = fields.Boolean("Estatus")
    contrasts_property = fields.Many2one('property.leasing', 'Inmueble')
    payment_id = fields.One2many('payment.property' , 'contract_id',string='pagos')

# Mantenimiento
    currency_id      =  fields.Many2one('res.currency', string='Moneda')
    maintenance      =  fields.Monetary(string="Mantenimiento", currency_field='currency_id')
# Garantia
    currency_id_otro =  fields.Many2one('res.currency', string='Moneda')
    guarantee        =  fields.Monetary( string="Garantía",currency_field='currency_id')
# Mensualidad
    currency_id_two  =  fields.Many2one('res.currency', string='Moneda')
    monthly_payment  =  fields.Monetary( string="Mensualidad",currency_field='currency_id_two')
#Number contract

    @api.onchange('contrasts_property')
    def _set_name_contract(self):
         for record in self: 
            record.name = "CT/%s/%s/%s" %(time.strftime("%Y"), record.contrasts_property.name ,len(record.contrasts_property.contrasts_property))
 
